from rest_framework.response import Response
from rest_framework import viewsets, status
from .models import CustomUser
from rest_framework.permissions import IsAuthenticated
import random, requests, string

# Create your views here.
class GetUserInfoByDNIViewSet(viewsets.ViewSet):

    #permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        dni = request.GET.get('dni')
        if dni:
            user = CustomUser.objects.using('autentica').filter(dni = dni).first()
            if user:
                data = {}
                data['dni'] = user.dni
                data['cuit'] = user.cuit
                data['first_name'] = user.first_name
                data['last_name'] = user.last_name
                data['email'] = user.email
                data['gender'] = user.gender
                data['phone'] = user.phone
                data['birth_date'] = user.birth_date
                data['gender'] = user.gender
                data['postal_code'] = user.postal_code
                data['street'] = user.street
                data['street_info'] = user.street_info                        
                data['height'] = user.height
                data['floor'] = user.floor
                data['apartment'] = user.apartment
                data['is_municipal'] = user.is_municipal
                data['email_validated'] = user.email_validated
                return Response(data = data, status = status.HTTP_200_OK)
            else:
                return Response(data = {'detail': 'User not found.'}, status = status.HTTP_404_NOT_FOUND)
        else:
            return Response(data = {'detail': 'User not found.'}, status = status.HTTP_404_NOT_FOUND)

class GetUserInfoByCUITViewSet(viewsets.ViewSet):

    #permission_classes = [IsAuthenticated]

    def list(self, request, *args, **kwargs):
        cuit = request.GET.get('cuit')
        if cuit:
            user = CustomUser.objects.using('autentica').filter(cuit = cuit).first()
            if user:
                data = {}
                data['dni'] = user.dni
                data['cuit'] = user.cuit
                data['first_name'] = user.first_name
                data['last_name'] = user.last_name
                data['email'] = user.email
                data['gender'] = user.gender
                data['phone'] = user.phone
                data['birth_date'] = user.birth_date
                data['gender'] = user.gender
                data['postal_code'] = user.postal_code
                data['street'] = user.street
                data['street_info'] = user.street_info                        
                data['height'] = user.height
                data['floor'] = user.floor
                data['apartment'] = user.apartment
                data['is_municipal'] = user.is_municipal
                data['email_validated'] = user.email_validated
                return Response(data = data, status = status.HTTP_200_OK)
            else:
                return Response(data = {'detail': 'User not found.'}, status = status.HTTP_404_NOT_FOUND)
        else:
            return Response(data = {'detail': 'User not found.'}, status = status.HTTP_404_NOT_FOUND)

class SendEmailViewSet(viewsets.ViewSet):

    def list(self, request):
        dni = request.GET.get('dni')
        if dni:
            user = CustomUser.objects.using('autentica').filter(dni = dni).first()
            pin = self.generate_pin()
            mail = self.email_activation(pin, user.email)
            print(mail)
            return Response(data = mail, status = status.HTTP_200_OK)
        else:
            return Response(data = {'detail': 'User not found.'}, status = status.HTTP_404_NOT_FOUND)

    @staticmethod
    def generate_pin():
        random_pin = random.randint(100000,999999)
        user_pin = random_pin
        return user_pin
    
    @staticmethod
    def send_pin_whatsapp(pin, phone, first_name):
            url = 'http://128.0.202.248:3000/2faauthentication'
            phone = phone
            first_name = first_name
            pin = pin
            data = {'celular': phone, 'nombre': first_name, 'pin': pin}
            request = requests.post(url, json = data)
            return request
    
    @staticmethod
    def email_activation(pin, email):
        data = {
            'app_name': 'autentica',
            'api_key': 'P1FaWfL4o0Hih3TUqXmgIyRK9YZdOtcVCMbJñvzeNSlx2GrABp8EuDkQjsw5',
            'subject': 'VALIDÁ TU EMAIL',
            'receiver': email,
            'message': f'<h1>Para activar tu email, hacé click <a href="http://localhost:1500/validate_email/?pin={pin}">ACÁ</a></h1>'
        }
        msg = requests.post(url = 'http://128.0.204.34:8492/email-delivery/', data = data)
        return msg

class ValidateEmailViewSet(viewsets.ViewSet):
    """
    Activación de mail: Dada una key que se ha enviado al mail y recuperado desde el front, se valida el email que la misma tenga asociada.
    """

    def list(self, request, *args, **kwargs):
        pin = request.GET.get('pin')
        if pin:
            user = CustomUser.objects.using('autentica').filter(email_key = pin).first()
            if user and user.email_validated == False:
                user.email_validated = True
                user.save()
                return Response(data = {'detail': 'Email validado satisfactoriamente.'}, status = status.HTTP_200_OK)
            else:
                return Response(data = {'detail': 'User not found or email already validated.'}, status = status.HTTP_404_NOT_FOUND)
        else:
            return Response(data = {'detail': 'Key incorrecta o email ya validado.'}, status = status.HTTP_403_FORBIDDEN)
    
class SendSMSViewSet(viewsets.ViewSet):

    def list(self, request):
        dni = request.GET.get('dni')
        if dni:
            user = CustomUser.objects.using('autentica').filter(dni = dni).first()
            if user:
                pin = self.generate_pin()
                sms = self.send_pin_whatsapp(pin, user.phone, user.first_name)
                if sms.status_code == 200:
                    return Response(data = {'detail': 'SMS ENVIADO.'}, status = status.HTTP_200_OK)
                else:
                    return Response(data = {'detail': 'ERROR.'}, status = status.HTTP_404_NOT_FOUND)
            else:
                return Response(data = {'detail': 'User not found.'}, status = status.HTTP_404_NOT_FOUND)
        else:
            return Response(data = {'detail': 'User not found.'}, status = status.HTTP_404_NOT_FOUND)
    
    @staticmethod
    def generate_pin():
        random_pin = random.randint(100000,999999)
        user_pin = random_pin
        return user_pin
    
    @staticmethod
    def send_pin_whatsapp(pin, phone, first_name):
        new_phone = str(phone).replace('-', '')
        print(phone, "-----", new_phone)
        data = {
            'recipient': new_phone,
            'message': f'Hola {first_name}, tu PIN para AUTENTICA es: {pin} - MBB'
        }
        msg = requests.post(url = 'http://128.0.204.34:8492/sms-delivery/', data = data)
        return msg

        # url = 'http://128.0.202.248:3000/2faauthentication'
        # phone = phone
        # first_name = first_name
        # pin = pin
        # data = {'celular': phone, 'nombre': first_name, 'pin': pin}
        # request = requests.post(url, json = data)
        # return request