from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models

from .managers import CustomUserManager

class CustomUser(AbstractBaseUser):
    dni = models.IntegerField(unique = True)
    password = models.TextField(max_length = 50, default = '')
    first_name = models.TextField(max_length=50, default = 'Jon')
    last_name = models.TextField(max_length=50, default = 'Doe')
    cuit = models.BigIntegerField(unique = True, null = True)
    email = models.EmailField(default = "", null = True)
    backup_email = models.EmailField(default = "", null = True)
    phone = models.CharField(null = True, max_length=25)
    backup_phone = models.CharField(null = True, max_length=25)
    birth_date = models.DateField(null=True)
    gender = models.CharField(max_length=1, null=True)
    street = models.CharField(max_length=50, null=True)
    street_code = models.IntegerField(null = True)
    height = models.IntegerField(null=True)
    apartment = models.CharField(max_length=10, null=True)
    floor = models.CharField(max_length=10, null=True)
    postal_code = models.TextField(null = True)
    level = models.CharField(null=True, max_length=1)
    is_completed = models.BooleanField(default = False)
    is_active = models.BooleanField(default=False)
    lon = models.CharField(max_length=255, null=True)
    lat = models.CharField(max_length=255, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_staff = models.BooleanField(default = False)
    is_municipal = models.BooleanField(default = False)
    token_info = models.TextField(null = True)
    email_key = models.TextField(null = True)
    email_validated = models.BooleanField(default = False, null = True)
    street_info = models.TextField(null = True)

    USERNAME_FIELD = 'dni'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'email', 'password']

    objects = CustomUserManager()
    
    def has_module_perms(self, app_label):
        return self.is_staff

    def has_perm(self, perm, obj=None):
        return self.is_staff

    def __str__(self):
        return str(self.dni)
