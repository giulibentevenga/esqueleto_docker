from rest_framework.routers import DefaultRouter
from django.contrib import admin
from django.urls import path, include

from .user.views import GetUserInfoByDNIViewSet, GetUserInfoByCUITViewSet, SendEmailViewSet, ValidateEmailViewSet, SendSMSViewSet

routes = DefaultRouter()    

routes.register(r'getuserinfobydni', GetUserInfoByDNIViewSet, basename='getuserinfobydni')
routes.register(r'getuserinfobycuit', GetUserInfoByCUITViewSet, basename='getuserinfobycuit')
routes.register(r'sendemail', SendEmailViewSet, basename='sendemail')
routes.register(r'sendsms', SendSMSViewSet, basename='sendsms')
routes.register(r'validate_mail', ValidateEmailViewSet, basename='validate_email')

urlpatterns = [
    path('', include(routes.urls)),
    path('admin/', admin.site.urls),
]
